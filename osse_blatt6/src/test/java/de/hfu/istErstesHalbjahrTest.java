package de.hfu;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
import static de.hfu.unit.Util.*;

/**
 * Beispiele für Unit-Tests
 */
@DisplayName("Test der Klasse App")
public class istErstesHalbjahrTest {

    /**
     * Wird einmal vor allen Tests dieser Klasse aufgerufen
     */
    @BeforeAll
    static void initAll() {
    }

    /**
     * Wird jeweils vor den Tests dieser Klasse aufgerufen
     */
    @BeforeEach
    void init() {
    }

    /**
     * Wird immer erfolgreich sein
     */
    @Test
    void aequalenzklassefuertrue() {
        assertTrue(istErstesHalbjahr(1));
        assertTrue(istErstesHalbjahr(6));

    }
    @Test
    void aequalenzklassefuerfalse() {

        assertFalse(istErstesHalbjahr(7)); //hier war der Fehler
        assertFalse(istErstesHalbjahr(12));


    }
    @Test
    void aequalenzklassefuerausserhalb() {

        assertThrows(IllegalArgumentException.class, () -> {
            istErstesHalbjahr(0);
            istErstesHalbjahr(13);
        }, "IllegalArgumentException was expected");

    }
    /**
     * Wird jeweils nach den Tests dieser Klasse aufgerufen
     */
    @AfterEach
    void tearDown() {
    }

    /**
     * Wird einmal nach allen Tests dieser Klasse aufgerufen
     */
    @AfterAll
    static void tearDownAll() {
    }
}
