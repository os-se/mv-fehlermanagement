package de.hfu;


import de.hfu.integration.*;
import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.repository.ResidentRepositoryStub;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;
import org.junit.jupiter.api.*;

import static org.easymock.EasyMock.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;




public class ResidentRepositoryTest {
    @Test
    public void getFilteredResidentsListTest(){
        BaseResidentService test = new BaseResidentService();
        ResidentRepository repository = new ResidentRepositoryStub();
        test.setResidentRepository(repository);
        List<Resident> residentList = test.getFilteredResidentsList(new Resident("M*", "", "", "", new Date(0000-00-00)));
        residentList.add(new Resident("Eris","Dincsoy","Musterbaum 4","Furtwangen", new Date(2000-03-17)));
        assertEquals(4,residentList.size());
        residentList = test.getFilteredResidentsList(new Resident("Hans", "Müller", "Musterweg 3", "München", new Date(1975-12-24)));
        assertEquals("Hans",residentList.get(0).getGivenName());
        residentList = test.getFilteredResidentsList(new Resident("Julia", "S*", "Musterstraße 8", "H*", new Date(1992-07-07)));
        assertEquals("Hannover",residentList.get(0).getCity());

    }
    @Test
    public void getFilteredResidentsListMockTest(){
        BaseResidentService test = new BaseResidentService();
        List<Resident> list = new ArrayList<>();

        Resident res1 = new Resident("Julia", "Schwarz", "Musterstraße 8", "Hannover", new Date(1992-07-07));
        Resident res2 = new Resident("Michael", "Wagner", "Musterweg 9", "Leipzig", new Date(1982-02-28));
        Resident res3 = new Resident("Hans", "Müller", "Musterweg 3", "München", new Date(1975-12-24));

        list.add(res1);
        list.add(res2);
        list.add(res3);

        ResidentRepository repositoryMock = createMock(ResidentRepository.class);
        expect(repositoryMock.getResidents()).andReturn(list).times(2);
        replay(repositoryMock);
        test.setResidentRepository(repositoryMock);
        //Hamcrest:
        List<Resident> residentList = test.getFilteredResidentsList(new Resident("M*", "", "", "", new Date(0000-00-00)));
        residentList.add(new Resident("Eris","Dincsoy","Musterbaum 4","Furtwangen", new Date(2000-03-17)));
        assertTrue(residentList.size()==2);
        residentList = test.getFilteredResidentsList(new Resident("Hans", "Müller", "Musterweg 3", "München", new Date(1975-12-24)));
        assertThat(residentList.get(0).getGivenName(),equalTo("Hans"));

        verify(repositoryMock);


    }
    @Test
    public void getUniqueResidentTest() {
        BaseResidentService test1 = new BaseResidentService();
        ResidentRepository repository = new ResidentRepositoryStub();
        test1.setResidentRepository(repository);
        Resident testResident = new Resident("Hans", "Müller", "Musterweg 3", "München", new Date(1975 - 12 - 24));
        try {
            Resident testResident1 = test1.getUniqueResident(testResident);
            assertEquals(testResident1.getGivenName(), "Hans");
            assertEquals(testResident1.getFamilyName(), "Müller");
            assertEquals(testResident1.getDateOfBirth(), testResident.getDateOfBirth());
        } catch (ResidentServiceException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    public void getUniqueResidentEasyMockTest(){
        BaseResidentService test = new BaseResidentService();
        List<Resident> list = new ArrayList<>();

        Resident res1 = new Resident("Julia", "Schwarz", "Musterstraße 8", "Hannover", new Date(1992-07-07));
        Resident res2 = new Resident("Michael", "Wagner", "Musterweg 9", "Leipzig", new Date(1982-02-28));


        list.add(res1);
        list.add(res2);

        ResidentRepository repositoryMock = createMock(ResidentRepository.class);
        expect(repositoryMock.getResidents()).andReturn(list).times(2);
        //damit das Verhalten 2 mal erwartet werden kann
        replay(repositoryMock);
        test.setResidentRepository(repositoryMock);

        try {
            Resident testResident1 = test.getUniqueResident(res1);
            assertEquals(testResident1.getGivenName(), "Julia");
            //Hamcrest:
            assertThat(testResident1.getFamilyName(), equalTo("Schwarz"));
            Resident testResident2 = test.getUniqueResident(res2);
            assertTrue(testResident2.getDateOfBirth() == res2.getDateOfBirth());
        } catch (ResidentServiceException e) {
            throw new RuntimeException(e);
        }

        verify(repositoryMock);
    }



}

