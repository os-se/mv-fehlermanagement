package de.hfu;



import de.hfu.unit.Queue;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;




/**
 * Beispiele für Unit-Tests
 */
@DisplayName("Test der Klasse App")
public class QueueTest {
    /**
     * Wird einmal vor allen Tests dieser Klasse aufgerufen
     */
    @BeforeAll
    static void initAll() {
    }

    /**
     * Wird jeweils vor den Tests dieser Klasse aufgerufen
     */
    @BeforeEach
    void init() {
    }

    /**
     * Wird immer erfolgreich sein
     */
    @Test
    void QueueTestConstructor() {
        assertThrows(IllegalArgumentException.class, () -> {
            de.hfu.unit.Queue queue1 = new Queue(-1);
            de.hfu.unit.Queue queue2 = new Queue(0);
        }, "IllegalArgumentException was expected");
    }
    @Test
    void QueueTestDequeue() {
        de.hfu.unit.Queue test1 = new Queue(3);
        test1.enqueue(1);
        test1.enqueue(2);
        test1.enqueue(3);

        assertEquals(test1.dequeue(),1);
        assertEquals(test1.dequeue(),2);
        assertEquals(test1.dequeue(),3);


    }
    @Test
    void EndOfQueueTest(){
        de.hfu.unit.Queue test1 = new Queue(3);
        test1.enqueue(1);
        assertThrows(IllegalStateException.class, () -> {
            test1.dequeue();
            test1.dequeue();
            }, "IllegalStateException was expected");
    }
    @Test
    void TopOfQueueTest(){
        de.hfu.unit.Queue test1 = new Queue(3);
        test1.enqueue(1);
        test1.enqueue(2);
        test1.enqueue(3);
        test1.enqueue(4);
        test1.enqueue(5);
        assertEquals(1,test1.dequeue());
        assertEquals(2,test1.dequeue());
        assertEquals(5,test1.dequeue());
    }
    /**
     * Wird jeweils nach den Tests dieser Klasse aufgerufen
     */
    @AfterEach
    void tearDown() {
    }

    /**
     * Wird einmal nach allen Tests dieser Klasse aufgerufen
     */
    @AfterAll
    static void tearDownAll() {
    }
}
