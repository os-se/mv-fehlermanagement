package de.hfu;
import java.util.Scanner;
/**
 * Hauptklasse des Praktikumprojekts
 */
public class App {

    public static void main( String[] args ) {
        Scanner s = new Scanner(System.in);
        String Eingabe = s.nextLine();
        System.out.println(Eingabe.toUpperCase());
        s.close();
    }
}